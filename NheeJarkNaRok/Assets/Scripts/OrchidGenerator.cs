﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrchidGenerator : MonoBehaviour
{
    public ObjectPooler orchidPool;

    public float distanceBetweenOrchid;

    public void SpawnOrchid(Vector3 startPosition)
    {
        GameObject orchid1 = orchidPool.GetPooledObject();
        orchid1.transform.position = startPosition;
        orchid1.SetActive(true);
        
        GameObject orchid2 = orchidPool.GetPooledObject();
        orchid2.transform.position = new Vector3(startPosition.x - distanceBetweenOrchid, startPosition.y, startPosition.z);
        orchid2.SetActive(true);
        
        GameObject orchid3 = orchidPool.GetPooledObject();
        orchid3.transform.position = new Vector3(startPosition.x + distanceBetweenOrchid, startPosition.y, startPosition.z);
        orchid3.SetActive(true);
    }
    
}
