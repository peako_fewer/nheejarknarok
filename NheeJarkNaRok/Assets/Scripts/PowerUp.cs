﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PowerUp : MonoBehaviour
{
    public bool doublePoint;

    public bool safeMode;

    public bool noPoint;

    public bool moreDanger;

    public float powerLenght;

    private PowerUpManager thePowerUpManager;

    public Sprite[] powerSprites;
        // Start is called before the first frame update
    void Start()
    {
        thePowerUpManager = FindObjectOfType<PowerUpManager>();
    }

    // Update is called once per frame
    void Awake()
    {
        int powerSelector = Random.Range(0, 2);

        switch (powerSelector)
        {
            case 0 : doublePoint = true;
                break;
            case 1 : safeMode = true;
                break;
            case 2 : noPoint = true;
                break;
            case 3 : moreDanger = true;
                break;
        }

        GetComponent<SpriteRenderer>().sprite = powerSprites[powerSelector];
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            thePowerUpManager.ActivatePower(doublePoint,safeMode,powerLenght,noPoint,moreDanger);
        }
        gameObject.SetActive(false);
    }
}
