﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    private bool doublePoint;

    private bool safeMode;

    private bool noPoint;

    private bool moreDanger;

    private bool powerActive;

    private float powerLenghtCounter;
    
    private ScoreManager theScoreManager;
    private PlatformGenerator thePlatformGenerator;
    private GameManager theGameManager;

    private float normalPointPerSecond;
    private float spikeRate;

    private PlatformDestroyer[] spikeList;


    // Start is called before the first frame update
    void Start()
    {
        theScoreManager = FindObjectOfType<ScoreManager>();
        thePlatformGenerator = FindObjectOfType<PlatformGenerator>();
        theGameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (powerActive)
        {
            powerLenghtCounter -= Time.smoothDeltaTime;

            if (theGameManager.powerReset)
            {
                powerLenghtCounter = 0f;
                theGameManager.powerReset = false; 
            }

            if (doublePoint&&theScoreManager.pointPerSecond<=2)
            {
                theScoreManager.pointPerSecond = normalPointPerSecond * 2f;
                theScoreManager.isDouble = true;
            }
            
            if (safeMode)
            {
                thePlatformGenerator.randomSpikeThreshold = 0f; 
            }

            if (noPoint)
            {
                theScoreManager.pointPerSecond = normalPointPerSecond * 0f;
            }

            if (moreDanger)
            {
                thePlatformGenerator.randomSpikeThreshold = 100f;
            }
            
            if (powerLenghtCounter <= 0)
            {
                theScoreManager.pointPerSecond = normalPointPerSecond;
                theScoreManager.isDouble = false;
                thePlatformGenerator.randomSpikeThreshold = spikeRate;
                safeMode = false;
            }
        }
    }

    public void ActivatePower(bool point,bool safe,float time,bool lessScore,bool danger)
    {
        doublePoint = point;
        safeMode = safe;
        noPoint = lessScore;
        moreDanger = danger;
        powerLenghtCounter = time;

        normalPointPerSecond = theScoreManager.pointPerSecond;
        spikeRate = thePlatformGenerator.randomSpikeThreshold;

        if (safeMode)
        {
            spikeList = FindObjectsOfType<PlatformDestroyer>();
            for (int i=0;i<spikeList.Length;i++)
            {
                if (spikeList[i].gameObject.name.Contains("spike 1"))
                {
                    spikeList[i].gameObject.SetActive(false);
                }
            }
        }

        powerActive = true;


    }
}
