﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public Text highScoreText;

    public float scoreCount;
    public float highScoreCount;

    public float pointPerSecond;

    public bool scoreIncreasing;

    public bool isDouble;

    public bool isNoPoint;
    
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("theHighScore"))
        {
            highScoreCount = PlayerPrefs.GetFloat("theHighScore");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scoreIncreasing)
        {
            scoreCount += pointPerSecond * Time.smoothDeltaTime;
        }

        if (scoreCount>highScoreCount)
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetFloat("theHighScore", highScoreCount);
        }
        
        scoreText.text = "Score: " + Mathf.Round(scoreCount);
        highScoreText.text = "High Score: " + Mathf.Round(highScoreCount);
    }

    public void AddScore(int pointToAdd)
    {
        if (isDouble)
        {
            pointToAdd = pointToAdd * 2;
        }

        if (isNoPoint)
        {
            pointToAdd = pointToAdd * 0;
        }
        scoreCount += pointToAdd;
    }
}
